﻿namespace StickerLand {
  
  /// <summary>
  /// Three words used for location finding
  /// </summary>
  public struct ThreeWords {

    /// <summary>
    /// First word
    /// </summary>
    public readonly string first;

    /// <summary>
    /// Second word
    /// </summary>
    public readonly string second;

    /// <summary>
    /// Third word
    /// </summary>
    public readonly string third;

    public ThreeWords(string first, string second, string third) {
      this.first = first;
      this.second = second;
      this.third = third;
    }

    public static implicit operator ThreeWords((string, string, string) words) {
      return new ThreeWords(words.Item1, words.Item2, words.Item3);
    }

    public void Deconstruct(out string first, out string second, out string third) {
      first = this.first;
      second = this.second;
      third = this.third;
    }

    public override int GetHashCode() {
      return (first + second + third).GetHashCode();
    }
  }
}