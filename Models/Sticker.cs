﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace StickerLand {

  /// <summary>
  /// A sticker to stick on locations
  /// </summary>
  public class Sticker {

    /// <summary>
    /// Status of this sticker in a user's collection
    /// </summary>
    public enum CollectionStatus {
      Unknown,
      Seen,
      Collected
    }

    /// <summary>
    /// The unique id of this sticker
    /// </summary>
    public string id {
      get;
      protected set;
    }

    /// <summary>
    /// The sticker type
    /// </summary>
    public Type type {
      get;
      private set;
    }

    /// <summary>
    /// Sticker placement and creation history
    /// </summary>
    public List<StickerHistoryEvent> history
      = new List<StickerHistoryEvent>();

    /// <summary>
    /// Make a new sticker of the given type:
    /// </summary>
    /// <param name="type"></param>
    public Sticker(User creator, Type type) {
      id = Guid.NewGuid().ToString();
      this.type = type;
      history.Add(new StickerHistoryEvent(StickerHistoryEvent.Type.Created, creator.name, null));
    }

    /// <summary>
    /// Pick back up a placed sticker
    /// </summary>
    public Sticker(PlacedSticker sticker, User pickedUpBy)
      // copy sticker base values:
      : this(
        sticker.id,
        sticker.type
    ) {
      if (sticker is PlacedSticker placedSticker && placedSticker.isStuck) {
        throw new Exception($"Cannot move or pick up a sticker that has already been stuck somewhere");
      }

      // update sticker history
      history = sticker.history;
      history.Add(
        new StickerHistoryEvent(
          StickerHistoryEvent.Type.PickedUp,
          pickedUpBy.name,
          history.Last().locationId
        )
      );
    }

    /// <summary>
    /// Make a sticker from an existing one:
    /// </summary>
    /// <param name="type"></param>
    protected Sticker(string id, Type type) {
      this.id = id;
      this.type = type;
    }

    /// <summary>
    /// A type of sticker. Flyweight pattern
    /// </summary>
    public class Type {

      /// <summary>
      /// The unique id of this sticker
      /// </summary>
      public string id {
        get;
        private set;
      }

      /// <summary>
      /// Name of the sticker
      /// </summary>
      public string name {
        get;
        private set;
      }

      /// <summary>
      /// Creator of the sticker
      /// </summary>
      public User creator {
        get;
        private set;
      }

      /// <summary>
      /// Artist credit or link
      /// </summary>
      public string artistCredit {
        get;
        private set;
      }

      public override int GetHashCode() {
        return id.GetHashCode();
      }
    }
  }

  /// <summary>
  /// A sticker history event
  /// </summary>
  public struct StickerHistoryEvent {
    public enum Type {
      Created,
      Placed,
      PickedUp
    }

    /// <summary>
    /// Type of event
    /// </summary>
    public readonly Type type;

    /// <summary>
    /// Id of the user who initiated the event
    /// </summary>
    public readonly string username;

    /// <summary>
    /// Location where the event took place
    /// </summary>
    public readonly ThreeWords? locationId;

    /// <summary>
    /// When this event occured
    /// </summary>
    public readonly DateTime time;

    public StickerHistoryEvent(Type type, string username, ThreeWords? locationId) {
      this.type = type;
      this.username = username;
      this.locationId = locationId;
      time = DateTime.Now;
    }
  }
}
