﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace StickerLand {

  /// <summary>
  /// A stack of multiple stickers of different types.
  /// Despite it's name, taking takes from the bottom, not top of the stack, so it's more like a queue
  /// </summary>
  public class StickerStack : IEnumerable<Sticker>  {

    /// <summary>
    /// All stickers in this stack by type
    /// </summary>
    protected QueueDictionary<Sticker.Type, QueueDictionary<string, Sticker>> stickersByType
      = new QueueDictionary<Sticker.Type, QueueDictionary<string, Sticker>>();

    /// <summary>
    /// The type of the sticker on top of the stack
    /// Helpful if it's known to be a stack of one type of sticker
    /// TODO: make this a value that's set on creation, or by the first item added, and lock it to that type if it's provided. Maybe make a "mixed" stack extension of this for that logic
    /// </summary>
    public virtual Sticker.Type topType
      => stickersByType.Keys.FirstOrDefault();

    /// <summary>
    /// Get all by type
    /// </summary>
    public IEnumerable<Sticker> this[Sticker.Type type] 
      => stickersByType[type].Values;

    /// <summary>
    /// Get one by ID
    /// </summary>
    public Sticker this[string stickerId]
      => this.SingleOrDefault(sticker => sticker.id == stickerId);

    /// <summary>
    /// Add a sticker
    /// </summary>
    public virtual void add(Sticker sticker) {
      if (stickersByType.TryGetValue(sticker.type, out var stickersOfThisType)) {
        stickersOfThisType.Add(sticker.id, sticker);
      } else {
        stickersByType.Add(sticker.type, new QueueDictionary<string, Sticker> { { sticker.id, sticker } });
      }
    }

    /// <summary>
    /// Add multiple stickers
    /// </summary>
    /// <param name="stickers"></param>
    public void addRange(IEnumerable<Sticker> stickers) {
      foreach(Sticker sticker in stickers) {
        Add(sticker);
      }
    }

    /// <summary>
    /// Split this stack up by type
    /// </summary>
    /// <returns></returns>
    public virtual IEnumerable<SingleTypeStickerStack> split() {
      List<SingleTypeStickerStack> parts = new List<SingleTypeStickerStack>();
      foreach((Sticker.Type stickerType, List<Sticker> stacks) in stickersByType as IEnumerable<KeyValuePair<Sticker.Type, List<Sticker>>>) {
        parts.Add(new SingleTypeStickerStack(stickerType, stacks));
      }

      return parts;
    }

    /// <summary>
    /// Take X stickers from the stack
    /// </summary>
    public StickerStack take(int quantityToFetch = 1) {
      StickerStack removedStickers = new StickerStack();
      while (quantityToFetch > 0 && stickersByType.Any()) {
        // if this type of stickers is empty, remove it and move to the next:
        if (!stickersByType.Next.Any()) {
          stickersByType.Dequeue();
        } // else remove the last sticker from the current stack of them: 
        else {
          removedStickers.add(stickersByType.Next.Dequeue());
          quantityToFetch--;
        }
      }

      return removedStickers;
    }

    #region Enumerable and list Stuff

    public void Add(Sticker sticker)
    => add(sticker);

    public IEnumerator<Sticker> GetEnumerator()
      => stickersByType.Values.SelectMany(stickersById => stickersById.Values).GetEnumerator();

    IEnumerator IEnumerable.GetEnumerator()
      => GetEnumerator();

    public override int GetHashCode() {
      return topType.GetHashCode();
    }

    #endregion
  }

  /// <summary>
  /// Multiple stacks into one
  /// </summary>
  public static class StickerStackExtensions {
    public static StickerStack Flatten(this IEnumerable<StickerStack> stacks) {
      StickerStack newStack = new StickerStack();
      foreach(StickerStack oldStack in stacks) {
        foreach(Sticker sticker in oldStack) {
          newStack.add(sticker);
        }
      }

      return newStack;
    }
  }

  /// <summary>
  /// Stack locked to a single type
  /// </summary>
  public class SingleTypeStickerStack : StickerStack {

    /// <summary>
    /// Override this to be easier
    /// </summary>
    public override Sticker.Type topType
      => _type;
    protected Sticker.Type _type;

    /// <summary>
    /// Make one with a set typee
    /// </summary>
    /// <param name="type"></param>
    public SingleTypeStickerStack(Sticker.Type type = null) {
      _type = type;
    }

    /// <summary>
    /// Make one with a set typee
    /// </summary>
    /// <param name="type"></param>
    internal SingleTypeStickerStack(Sticker.Type type, IEnumerable<Sticker> stickers) {
      _type = type;
      stickersByType[type] = new QueueDictionary<string, Sticker>(stickers.ToDictionary(
        sticker => sticker.id,
        sticker => sticker
      ));
    }

    /// <summary>
    /// Override add
    /// </summary>
    /// <param name="sticker"></param>
    public override void add(Sticker sticker) {
      if (_type == null) {
        _type = sticker.type;
      } else if (sticker.type != _type) {
        throw new System.Exception($"Tried to add a sticker of type {sticker.type.name}, to a stack locked to the type: {_type.name}");
      }
      base.add(sticker);
    }

    /// <summary>
    /// Change take's type
    /// </summary>
    /// <param name="quantityToFetch"></param>
    /// <returns></returns>
    public new SingleTypeStickerStack take(int quantityToFetch = 1) {
      SingleTypeStickerStack removedStickers = new SingleTypeStickerStack();
      while (quantityToFetch > 0 && stickersByType.Any()) {
        // if this type of stickers is empty, remove it and move to the next:
        if (!stickersByType.Next.Any()) {
          stickersByType.Dequeue();
        } // else remove the last sticker from the current stack of them: 
        else {
          removedStickers.add(stickersByType.Next.Dequeue());
          quantityToFetch--;
        }
      }

      return removedStickers;
    }

    public override IEnumerable<SingleTypeStickerStack> split() {
      return this.AsEnumerable();
    }
  }
}
