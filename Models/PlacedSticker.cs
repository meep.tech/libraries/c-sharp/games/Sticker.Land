﻿using System;

namespace StickerLand {

  /// <summary>
  /// A sticker that has been placed
  /// </summary>
  public class PlacedSticker : Sticker {

    /// <summary>
    /// If the sticker has been stuck to this location, or just left for someone to pick up
    /// </summary>
    public readonly bool isStuck;

    /// <summary>
    /// Make a new placed sticker from another sticker:
    /// </summary>
    public PlacedSticker(Sticker sticker, User placedBy, Location placedAt, bool isStuck = false)
      // copy sticker base values:
      : base(
        sticker.id,
        sticker.type
    ) {
      if (sticker is PlacedSticker placedSticker && placedSticker.isStuck) {
        throw new Exception($"Cannot move or re-place a sticker that has already been stuck somewhere");
      }
      // add new values
      this.isStuck = isStuck;

      // update sticker history
      history = sticker.history;
      history.Add(
        new StickerHistoryEvent(
          StickerHistoryEvent.Type.Placed,
          placedBy.name,
          placedAt.threeWords
        )
      );
    }
  }
}
