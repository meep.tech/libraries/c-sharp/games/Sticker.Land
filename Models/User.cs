﻿using System.Collections.Generic;
using System.Linq;

namespace StickerLand {

  /// <summary>
  /// A user of Sticker.Land
  /// </summary>
  public class User {

    /// <summary>
    /// UserName. This is the user's uniqueId
    /// </summary>
    public string name {
      get;
      private set;
    }

    /// <summary>
    /// User's Displayed Name
    /// </summary>
    public string displayName {
      get;
      private set;
    }

    /// <summary>
    /// The user's "Cred"
    /// Used as currency for making stickers.
    /// Gained from placing your own & other people's stickers, and walking
    /// </summary>
    public int cred {
      get;
      private set;
    }

    /// <summary>
    /// Stickers this user has created
    /// </summary>
    public IReadOnlyList<Sticker.Type> createdStickers {
      get;
      private set;
    }

    /// <summary>
    /// Stickers this user has placed indexed by their locations
    /// </summary>
    public IReadOnlyDictionary<ThreeWords, StickerStack> placedStickers {
      get;
      private set;
    } = new Dictionary<ThreeWords, StickerStack>();

    /// <summary>
    /// The user's sticker collection. Functions as inventory and empty ones function for 'seen/collected' stickers.
    /// </summary>
    public IReadOnlyDictionary<Sticker.Type, (SingleTypeStickerStack stickers, Sticker.CollectionStatus collectionStatus)> stickerCollection {
      get;
      private set;
    } = new Dictionary<Sticker.Type, (SingleTypeStickerStack, Sticker.CollectionStatus)>();

    /// <summary>
    /// Placeable stickers in the user's inventory
    /// </summary>
    public IEnumerable<StickerStack> stickerInventory
      => stickerCollection.Values.Where(
        // sticker has been collected and we have more than 0
        stickerTypeData => stickerTypeData.collectionStatus == Sticker.CollectionStatus.Collected
          && stickerTypeData.stickers.Count() > 0
      ).Select(sticker => sticker.stickers);

    /// <summary>
    /// Place stickers of a type from the inventory
    /// </summary>
    public void placeStickersFromInventory(Sticker.Type stickerType, Location @location, int quantity = 1, bool stickTheFirstOne = true) {
      placeStickers(stickerCollection[stickerType].stickers.take(quantity), @location, stickTheFirstOne);
    }

    /// <summary>
    /// Place stickers at a location
    /// </summary>
    public void placeStickers(SingleTypeStickerStack stickers, Location @location, bool stickTheFirstOne = true) {
      bool stickNextSticker = stickTheFirstOne;
      // first turn them into placed stickers:
      foreach (Sticker sticker in stickers) {
        bool stick = false;
        if (stickNextSticker) {
          stick = true;
          stickNextSticker = false;
        }

        // add each one to the location, and save that we added it to our placed stickers:
        addToPlacedStickers(location.placeSticker(sticker, this, stick));
      }
    }

    /// <summary>
    /// Pick up all the provided placed stickers and add them to the user's inventory
    /// </summary>
    public void pickUpStickers(StickerStack placedStickers) {
      StickerStack pickedUpStickers = new StickerStack();
      foreach(PlacedSticker placedSticker in placedStickers) {
        pickedUpStickers.add(new Sticker(placedSticker, this));
      }

      addToCollection(pickedUpStickers);
    }

    /// <summary>
    /// Add to sticker collection stack
    /// </summary>
    /// <param name="stickers"></param>
    void addToCollection(StickerStack stickers) {
      Dictionary<Sticker.Type, (SingleTypeStickerStack stickers, Sticker.CollectionStatus collectionStatus)>  updatedStickerCollections 
        = new Dictionary<Sticker.Type, (SingleTypeStickerStack stickers, Sticker.CollectionStatus collectionStatus)>();

      foreach(SingleTypeStickerStack stack in stickers.split()) {
        // if there's already been collected stickers of this type in the past:
        if (stickerCollection.TryGetValue(stack.topType, out (SingleTypeStickerStack stickers, Sticker.CollectionStatus collectionStatus) existingStickrCollectionData)) {
          stack.addRange(existingStickrCollectionData.stickers);
          Sticker.CollectionStatus updatedStatus = existingStickrCollectionData.collectionStatus < Sticker.CollectionStatus.Collected
            ? stack.Count() > 0
              ? Sticker.CollectionStatus.Collected
              : Sticker.CollectionStatus.Seen
            : Sticker.CollectionStatus.Seen;
          updatedStickerCollections[stack.topType] = (stack, updatedStatus);
        } // if this is a new sticker type:
        else {
          updatedStickerCollections[stack.topType] = (
            stack,
            stack.Count() > 0
              ? Sticker.CollectionStatus.Collected
              : Sticker.CollectionStatus.Seen
          );
        }
      }

      /// overrite any changed data:
      stickerCollection = stickerCollection.Merge(updatedStickerCollections);
    }

    /// <summary>
    /// Marks the sticker as seen if it hasn't been yet:
    /// </summary>
    /// <param name="stickerType"></param>
    void addToSeenStickers(Sticker.Type stickerType)
      => addToCollection(new SingleTypeStickerStack(stickerType));

    /// <summary>
    /// Place a sticker at the desiered location
    /// </summary>
    void addToPlacedStickers(PlacedSticker sticker) {
      StickerStack updatedStickerList = new StickerStack {
        sticker
      };

      /// if we have existing stickers in this location:
      ThreeWords locationPlacedAt = sticker.history.First().locationId.Value;
      if (placedStickers.TryGetValue(locationPlacedAt, out StickerStack stickersAtThisLocation)) {
        updatedStickerList.addRange(stickersAtThisLocation);
        // merge will overrite:
        placedStickers = placedStickers.Merge(new Dictionary<ThreeWords, StickerStack> {
          {locationPlacedAt, updatedStickerList }
        });
      } /// else add the new location:
      else {
        placedStickers = placedStickers.Prepend(new KeyValuePair<ThreeWords, StickerStack>(locationPlacedAt, updatedStickerList))
            .ToDictionary(x => x.Key, y => y.Value);
      }
    }

    /// <summary>
    /// Place a sticker at the desiered location
    /// </summary>
    void addToCreatedStickers(Sticker.Type newStickerType)
      => createdStickers = createdStickers
        .Prepend(newStickerType)
          .ToList();
  }
}
