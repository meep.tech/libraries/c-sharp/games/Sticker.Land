﻿using System;

namespace StickerLand {

  /// <summary>
  /// A Three words location square:
  /// </summary>
  public class Location {

    /// <summary>
    /// Three words adress of this location
    /// </summary>
    public ThreeWords threeWords {
      get;
    }

    /// <summary>
    /// The landmark at this location, if it has one
    /// </summary>
    public Landmark landmark {
      get;
      private set;
    }

    /// <summary>
    /// If this location has been designated a landmark
    /// </summary>
    public bool isALandmark
      => landmark != null;

    /// <summary>
    /// Make a location object based on a set of coordinates
    /// </summary>
    /// <param name="threeWords"></param>
    public Location(ThreeWords threeWords) {
      this.threeWords = threeWords;
    }

    /// <summary>
    /// Place a sticker at this location
    /// </summary>
    public PlacedSticker placeSticker(Sticker sticker, User placedBy, bool stick) {
      PlacedSticker placedSticker = new PlacedSticker(sticker, placedBy, this, stick);
      return placedSticker;
    }
  }

  /// <summary>
  /// A Named set of location tiles
  /// </summary>
  public class Landmark {

    /// <summary>
    /// Name of the landmark, set by it's discoverer
    /// </summary>
    public string name {
      get;
      private set;
    }

    /// <summary>
    /// Center location of this landmark
    /// </summary>
    public Location center;
  }
}
